#solving without using division. time complexity O(n), space O(n)
class Solution(object):
    def productExceptSelf(self, nums):
        output = [1]*len(nums) #used to create a list with the amount of element in the nums list
        print(output,"length:", len(nums))

        prefix = 1
        for i in range(len(nums)): #range will go from index 0 to 3
            output[i] = prefix
            prefix *= nums[i]
        print("Prefix:",prefix,"OUTPUT :", output)
        postfix = 1
        for i in range(len(nums)-1, -1, -1): #will start at the end(index 3)and go to the left all the way to index 0   #...-1 is the stop since we don't want to stop at 0!
            output[i] *= postfix  #on first loop it multiplies 6 times 1(postfix), second loop index2,2,*4(from postfix), third loop index1,1,*12(postfix), last one index0,1*24(postfix)
            postfix *= nums[i]    #on first loop postfix becomes 4,                second loop 4*=3(nums[2])=12,       third loop 12*2(nus[1])=24           last one 24*1 = 24
        print("Postfix:",postfix,"OUTPUT :", output)
        return output


solution = Solution()

# Define some input values
prices = [1,2,3,4]


# Call the twoSum method and print the result
result = solution.productExceptSelf(prices)
print(result) #returns the indeces
