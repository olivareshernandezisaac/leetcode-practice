# class Solution(object):
#     def isAnagram(self, s, t):
#         print(len(s))
#         total_lettersS = {}
#         total_lettersT = {}
#         if len(s) == len(t):
#             for letter in range(len(s)):
#                 if s[letter] not in total_lettersS:
#                     total_lettersS[s[letter]]=0
#                 total_lettersS[s[letter]]+=1
#                 if t[letter] not in total_lettersT:
#                     total_lettersT[t[letter]]=0
#                 total_lettersT[t[letter]]+=1
#             print(len(total_lettersS))
#             for i in  total_lettersS:
#                 print(total_lettersS[i])
#                 if total_lettersS[i] != total_lettersT[i]:
#                     return False
#             return True
#         else:
#             return False

# solution = Solution()

# s = "aacc"
# t = "ccac"
# result = solution.isAnagram(s,t)
# print(result)

#O(C+T) in memory and time
class Solution(object):
    def isAnagram(self, s, t):
        print(len(s))
        if len(s) != len(t):
            return False
        countS, countT = {}, {}
        for i in range(len(s)):  #bottom line counts the ocurrances of each character##BUILDING HASHMAPS
            countS[s[i]] = 1+ countS.get(s[i],0) #creates a default value of 0 if key does not exist yet...
            countS[t[i]] = 1+ countS.get(t[i],0)              #...if key(s[i]) does not exist in hashmap(countS), then it will have a value of 0
        for c in countS: #iterating thru key:values. countS[c] will compare the value to the value of countT[c]
            if countS[c] != countT.get(c, 0): #comparing the same keys in both dictionaries, will throw a default value of 0 if key does not exist
                return False
        return True

solution = Solution()

s = "aacc"
t = "ccac"
result = solution.isAnagram(s,t)
print(result)
