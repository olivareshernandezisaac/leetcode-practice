
class Solution(object):
    def maxProfit(self, prices):
        lista = {}
        lowest = prices[0]
        # print(lowest)
        for i, element in enumerate(prices): #enumerate gives us (index, element)
            # print(i, element) #firs loop will give (0,7), on the second (1,2)
            if lowest > element:
                lowest = element
                # print(lowest)
                lista[lowest]= i #will give us {2:1}, switched so it is now element:index
                # print(lista[lowest])
        index_lowest = lista[lowest] #gives us the index where the lowest number is, so 1 in this case
        # print(index_lowest)
        biggest = lowest #assign the lowest to be the biggest before the loop starts
        for digit in range(index_lowest+1,len(prices)): #will start with index 2, which is 5
            if prices[digit]>= biggest:
                biggest = prices[digit]
        return biggest-lowest



solution = Solution()

# Define some input values
prices = [7,6,4,3,1]


# Call the twoSum method and print the result
result = solution.maxProfit(prices)
print(result) #returns the indeces
