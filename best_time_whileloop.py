#using sliding window technique
class Solution(object):
    def maxProfit(self, prices):
        l, r = 0, 1
        maxP = 0

        while r< (len(prices)):
            if prices[l] < prices[r]:
                profit = prices[r]-prices[l]
                maxP = max(maxP, profit)
            else:
                l = r
            r +=1
        return maxP

solution = Solution()

# Define some input values
prices = [7,1,5,3,6,4]

# Call the twoSum method and print the result
result = solution.maxProfit(prices)
print(result) #returns the indeces


#using a for loop
class Solutio(object):
    def maxProfit(self, prices):
        ###
solutio = Solutio()

# Define some input values
prices = [7,1,5,3,6,4]

# Call the twoSum method and print the result
result = solutio.maxProfit(prices)
print(result) #returns the indeces
