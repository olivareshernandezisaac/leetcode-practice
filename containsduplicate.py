class Solution(object):
    def containsDuplicate(self, nums):
        newlist = {}
        for num in nums:
            if num not in newlist:
                newlist[num] = 0 #it will start a new key:value pair in the dictionary
            newlist[num] += 1 #it will find that key/value and add 1
        for value in newlist.values(): #we could also have used for key,value in newlist.items()
            print(value)
            if value >=2:
                return True
        return False

solution = Solution()
nums = [2,14,18,22,22]
result = solution.containsDuplicate(nums)
print(result)


