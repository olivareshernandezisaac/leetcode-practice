#Find the two numbers that will add up to the target

class Solution(object):
    def twoSum(self, nums, target):
        lista  = {}
        for i, num in enumerate(nums):
            if target - num in lista:
                print(lista[target-num])
                return [lista[target-num], i]
            lista[num] = i
        return []

# Create an instance of the Solution class
solution = Solution()

# Define some input values
nums = [2, 7, 11, 15]
target = 9

# Call the twoSum method and print the result
result = solution.twoSum(nums, target)
print(result) #returns the indeces
