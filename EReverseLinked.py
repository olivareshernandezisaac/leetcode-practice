#O(1) time complex
class Solution(object):
    def reverseList(self, lista):
        newlist = []
        for i in range(len(lista)-1,-1,-1): #start at last index,4, then stop before getting to -1, step -1 each time
            newlist.append(lista[i])
        return newlist


solution = Solution()

head = [1,2,3,4,5]
result = solution.reverseList(head)
print(result)
