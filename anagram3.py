#O(1) time complex
class Solution(object):
    def isAnagram(self, s, t):
        if sorted(s) == sorted(t):
            return True
        return False


solution = Solution()

s = "aacc"
t = "ccac"
result = solution.isAnagram(s,t)
print(result)
