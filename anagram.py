class Solution(object):
    def isAnagram(self, s, t):
        print(len(s))
        total_lettersS = {}
        total_lettersT = {}
        if len(s) == len(t):
            for letter in range(len(s)):
                if s[letter] not in total_lettersS:
                    total_lettersS[s[letter]]=0 #created the key and value if it is not existant
                total_lettersS[s[letter]]+=1 #increased value by 1 if the key exists
                if t[letter] not in total_lettersT:
                    total_lettersT[t[letter]]=0
                total_lettersT[t[letter]]+=1
            print(len(total_lettersS))
            if total_lettersS == total_lettersT:
                return True
            else:
                return False
        else:
            return False


solution = Solution()

s = "aacc"
t = "ccac"

result = solution.isAnagram(s,t)
print(result)
